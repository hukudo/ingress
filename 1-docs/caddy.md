# caddy
https://caddyserver.com/docs/

## Troubleshooting
```
{"error":"adapting config using caddyfile adapter: automation policy from site block is also default/catch-all policy because of key without hostname, and the two are in conflict: []certmagic.Issuer(nil) != []certmagic.Issuer{(*caddytls.InternalIssuer)(0xc00080c690)}"}
```
Add `local_certs` to global options.
