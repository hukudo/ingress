# Controller
- runs a control loop
  - inspect all containers
    - extract labels
      - `ingress.host` for the externally visible hostname
      - `ingress.port` for the port that the application listens on
- renders a Jinja2 template `Caddyfile`
  - can be overridden with `--template` argument or `INGRESS_TEMPLATE`
    environment variable
  - is given a ProxyMap (a list of Proxy instances)
