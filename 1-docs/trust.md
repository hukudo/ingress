# Trusting a Certificate Authority (CA)
Given the file `~/ingress/root.crt` file, here is how to trust it.

## Linux
### Debian / Ubuntu
```
sudo install --mode 644 ~/ingress/root.crt /usr/local/share/ca-certificates/ingress.crt
sudo update-ca-certificates
ls -l /etc/ssl/certs/ingress.pem  # the created symlink
```

### Arch Linux
```
sudo trust anchor --store ~/ingress/root.crt
```

## Firefox
Here is how to add a certificate authority manually:

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - click Import and select `~/ingress/root.crt`
  - set "Trust this CA to identify websites"

![Firefox: How to add a Certificate Authority][firefox-vid]

[firefox-vid]: https://gitlab.com/hukudo/devenv/devcerts/-/wikis/uploads/5d46e33ed7a3e7ae9b800f93091e6151/firefox--how-to-add-a-certificate-authority.webm

## Mozilla NSS
Mozilla's [Network Security Services][nss] is used in [many open source
projects][nss-overview].
```
hash certutil || sudo apt-get install libnss3-tools
certutil -d sql:$HOME/.pki/nssdb -A -n ingress -t "C,p,p" -i ~/ingress/root.crt
```

[nss]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS
[nss-overview]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Overview

## Chrome
- Settings > Manage Certificates > Authorities: Import
  - `~/ingress/root.crt`
  - "Trust this certificate for identifying websites"


# Removing Trust
```
certutil -d sql:$HOME/.pki/nssdb -D -n ingress

sudo rm /usr/local/share/ca-certificates/ingress.crt
sudo update-ca-certificates
```

Firefox:

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - select "Caddy Local Authority..."
  - click "Delete or Distrust"
  - confirm
