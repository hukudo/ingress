MAKEFLAGS += --always-make

default: ci-lint

ci-lint:
	gitlab -g gitlab.com --verbose project-ci-lint validate --project-id hukudo/ingress --content @.gitlab-ci.yml
