# Ingress for Docker Compose with TLS

You write a `docker-compose.yml` like this:
```yaml
networks:
  ingress:
    external: true

services:

  alpha:
    networks: [default, ingress]
    image: tenzer/http-echo-test
    expose:
      - "8080"
    labels:
      # set external hostname and use the first exposed port
      ingress.host: a.0-main.de
```

And your browser happily navigates you to https://a.0-main.de

For a complete example, see [](#usage-example).


# Prerequisites
[git](https://git-scm.com/),
[bash](https://www.gnu.org/software/bash/),
[docker](https://docs.docker.com/engine/install/),
[docker-compose](https://docs.docker.com/compose/install/) **v2**

Ports `80` and `443` must be free on your machine. To check this you can use
`netstat` or `lsof`:
```
sudo netstat -lnpt | egrep ':80|:443'
sudo lsof -Pn -iTCP -sTCP:LISTEN
```


# Usage
```
# clone
git clone https://gitlab.com/hukudo/ingress.git ~/ingress
cd ~/ingress

# create the ingress network
docker network create ingress

# start
docker-compose up -d --build --remove-orphans
```

# TODO
- make caddy generate a root cert before first use


# Trust Caddy Local CA
Copy Caddy's generated `root.crt` to `~/ingress`
```
docker cp ingress-caddy-1:/data/caddy/pki/authorities/local/root.crt ~/ingress/
```

## Firefox
Settings > Certs > Authorities > Import: `~/ingress/root.crt`

## Debian / Ubuntu
```
sudo install --mode 644 ~/ingress/root.crt /usr/local/share/ca-certificates/ingress.crt
sudo update-ca-certificates  # should say "1 added"
ls -l /etc/ssl/certs/ingress.pem  # the created symlink
```

See [1-docs/trust.md](1-docs/trust.md) for more.


# Usage Example
```
cd example/ && docker-compose up -d && cd -

curl https://a.0-main.de
curl https://b.0-main.de

cd example/ && docker-compose down && cd -
```

You need to include the `ingress` network and set correct labels, so Caddy
knows what to name your server (`ingress.host`) and on which port of the
container to route your requests (`ingress.port`).

Please refer to the scripts in [controller/](controller/).


# Logs
The latest mappings are written to the controller logs.
```
docker-compose logs -f --timestamps controller
```

Logs for a specific domain are written to files.
```
docker-compose exec caddy ls /log/
docker-compose exec caddy tail -f /log/a.0-main.de.json
```


# Notes

## ingress network
Docker-Compose creates a network per project, but we want the ingress to be
standalone. Thus, we create a dedicated `ingress` network and join other
project's main web server where needed.

## dns (optional)
CoreDNS returns 127.0.0.1 for any query, so you don't need to abuse
`/etc/hosts` and have a mess of `127.0.0.1 something blah` entries in there.

### NetworkManager
First, configure NetworkManager to use dnsmasq:
```
sudo vi /etc/NetworkManager/NetworkManager.conf
```

Content example:
```
[main]
plugins=ifupdown,keyfile
dns=dnsmasq
```

Next, configure NetworkManager's dnsmasq to use 127.0.0.1 on 1053 for the
domain `0-main.de` and reload.
```
sudo bash -i <<'ENDOFSUDO'
cat <<'EOF' > /etc/NetworkManager/dnsmasq.d/ingress.conf
server=/0-main.de/127.0.0.1#1053
EOF

systemctl reload NetworkManager.service
ENDOFSUDO
```

## Reserved TLDs for private use
Unfortunately there are no top level domains that are reserved for private use.
There was an [IETF draft][] in 2011 and people talk about it[^hn], but nothing
ever came of it.

Thus we registered and maintain the domain `0-main.de`. You can [read more
about 0-main.de](https://blog.hukudo.de/infra/0-main.html) on our blog.

[IETF draft]: https://www.ietf.org/archive/id/draft-chapin-rfc2606bis-00.html
[^hn]: See https://news.ycombinator.com/item?id=17093942 with discussion


# Backup & Restore
Simply backup/restore the `caddy_data` volume.


# Cheatsheet
Check Upstreams
```
curl -s http://localhost:2019/reverse_proxy/upstreams | jq
```

Reconfigure
```
docker-compose exec controller ingress reconfigure
```


# Development
Pre-Commit Hook:
```
ln -s \
  $(readlink -m controller/env/dev/git-hooks/pre-commit) \
  .git/hooks/pre-commit
```


# Authelia
- https://www.authelia.com/
- https://github.com/authelia/authelia/tree/master/examples/compose/local


# Duplicate Host Conflicts
If two different containers run the same `ingress.host` argument, an error page
is displayed and a warning is emitted.


# Troubleshooting

## `DNS_PROBE_FINISHED_NXDOMAIN`
If `dig 0-main.de +short` does NOT return `127.0.0.1`, then maybe you have a
router with [DNS Rebind Protection][rebind-protection] like AVM Fritz! Box. You
can deactivate it as [described
here](https://avm.de/service/wissensdatenbank/dok/FRITZ-Box-7490/3565_FRITZ-Box-meldet-Der-DNS-Rebind-Schutz-Ihrer-FRITZ-Box-hat-Ihre-Anfrage-aus-Sicherheitsgrunden-abgewiesen/).
