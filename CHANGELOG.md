# 2022-05

## Breaking Changes
You MUST use the label `ingress.host` to set the externally visible hostname.
See [example/docker-compose.yml](example/docker-compose.yml) for usage.

This avoids conflicts on the ingress network. Before, the name `foo.0-main.de`
would have been the actual container that exposed a port like `:8000`. When you
added a client to the ingress network and sent a request to
`https://foo.0-main.de` nothing would have listened there.

With this change, your container's hostname on the ingress network is its
primary network alias (defaulting to a container id, e.g. `372f88ac5355`.
When you set the label `ingress.host` to `foo.0-main.de` on this container,
then caddy renders a config snippet like this:
```
foo.0-main.de:80 {
	reverse_proxy 372f88ac5355:8983
	log {
		output file /log/foo.0-main.de.json
	}
	import errors
}
foo.0-main.de:443 {
	tls internal
	reverse_proxy 372f88ac5355:8983
	log {
		output file /log/foo.0-main.de.json
	}
	import errors
}
```

Thus, requesting https://foo.0-main.de always results in a response from Caddy,
no matter in which environment (your machine vs. inside a container in the
ingress network) the client is present.
