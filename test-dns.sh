#!/bin/bash
set -euo pipefail

dig_coredns() {
  dig @127.0.0.1 -p 1053 +timeout=1 "$@"
}

assert_ipv4() {
  [[ $(dig_coredns A +short $1) == "127.0.0.1" ]]
}

assert_ipv6() {
  [[ $(dig_coredns AAAA +short $1) == "::1" ]]
}

assert_ipv4 0-main.de
assert_ipv4 a.0-main.de
assert_ipv4 a.b.c.0-main.de

assert_ipv6 0-main.de
assert_ipv6 a.0-main.de
assert_ipv6 a.b.c.0-main.de
