#!/usr/bin/env python
# encoding: utf-8
import json
import logging
import os
import pprint
import shlex
import subprocess

# pip install logfmter
# https://pypi.org/project/logfmter/
from logfmter import Logfmter

command = 'docker-compose logs --tail=100 -f --no-color --no-log-prefix caddy'
process = subprocess.Popen(
    shlex.split(command),
    shell=False,
    stdout=subprocess.PIPE
)


handler = logging.StreamHandler()
# handler.setFormatter(Logfmter(
#     keys=['at', 'ts'],
#     mapping={"at": "levelname", "ts": "asctime"},
#     datefmt="%Y-%m-%dT%H:%M:%S%z"
# ))
handler.setFormatter(Logfmter(keys=['at'], mapping={"at": "levelname"}))
logging.basicConfig(
    handlers=[handler],
    level=os.environ.get('LOGLEVEL', 'INFO').upper()
)


def transform(d: dict) -> dict:
    """
    See example/log.dict
    """
    try:
        request = d['request']
        return {
            'host': request['host'],
            'method': request['method'],
            'status': d['status'],
            'path': request['uri'],
            'size': d['size'],
        }
    except KeyError:
        return d


while True:
    line = process.stdout.readline()
    if process.poll() is not None:
        break
    if line:
        try:
            d = json.loads(line)
            logging.info(transform(d))
        except json.JSONDecodeError:
            print(line)

rc = process.poll()
raise SystemExit(rc)
