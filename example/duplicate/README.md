This starts two different containers both having `ingress.host` set to
`dup.0-main.de`.

When navigating to https://dup.0-main.de you will see an error page.
