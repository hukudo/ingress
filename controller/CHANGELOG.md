# TODO
- (verbose using [rich](https://github.com/Textualize/rich#readme))

# 2022-08
- add an [aggregate][agg] for `[Proxy]` called `ProxyMap`
- simpler reconfigure
  - get rid of docker events
  - inspect every 1s
  - remember ProxyMap state
  - on diff, do reconfigure

[agg]: https://www.cosmicpython.com/book/chapter_07_aggregate.html
